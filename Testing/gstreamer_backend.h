//
//  gstreamer_backend.h
//  Testing
//
//  Created by Alann on 13/12/2017.
//  Copyright © 2017 Kalyzee. All rights reserved.
//

#ifndef gstreamer_backend_h
#define gstreamer_backend_h

#include <stdio.h>
#import <gst/gst.h>
#include <gst/video/video.h>

GstElement *gst_test_get_pipeline(void);

// Configuration
void gst_test_set_auto_play(gboolean _autoPlay);
void gst_test_set_drawable_surface(guintptr _drawableSurface);

// Loops
void gst_test_prepare_loop(gchar *uri);
void gst_test_run_loop();

#endif /* gstreamer_backend_h */
