//
//  EaglUiView.m
//  Testing
//
//  Created by Alann on 13/12/2017.
//  Copyright © 2017 Kalyzee. All rights reserved.
//

#import "EaglUiView.h"

@implementation EaglUiView

+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

@end
