//
//  ViewController.m
//  Testing
//
//  Created by Alann on 12/12/2017.
//  Copyright © 2017 Kalyzee. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    GstElement *pipeline;
    char *uri;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib
    
    self->uri = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov";
    guintptr drawableSurface = (guintptr)(id)self->_drawableView;
    
    gst_test_set_auto_play(TRUE);
    gst_test_set_drawable_surface(drawableSurface);
    
    gst_test_prepare_loop(self->uri);
    self->pipeline = gst_test_get_pipeline();
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        gst_test_run_loop();
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Buttons actions
- (IBAction)nullPressed:(id)sender withEvent:(UIEvent *)event {
    [self setState:GST_STATE_NULL];
}

- (IBAction)readyPressed:(id)sender withEvent:(UIEvent *)event {
    [self setState:GST_STATE_READY];
}

- (IBAction)pausedPressed:(id)sender withEvent:(UIEvent *)event {
    [self setState:GST_STATE_PAUSED];
}

- (IBAction)playingPressed:(id)sender withEvent:(UIEvent *)event {
    [self setState:GST_STATE_PLAYING];
}


// GST Interactions
- (void)setState:(GstState)state
{
    NSLog(@"Setting state to : %@", [NSString stringWithUTF8String:gst_element_state_get_name(state)]);
    
    gst_element_set_state(self->pipeline, state);
}

- (void)setWindow:(guintptr)window
{
    NSLog(@"Setting video window handle to %p", window);
    gst_test_set_drawable_surface(window);
}

@end
