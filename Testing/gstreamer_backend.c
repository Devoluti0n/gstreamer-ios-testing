//
//  gstreamer_backend.c
//  Testing
//
//  Created by Alann on 13/12/2017.
//  Copyright © 2017 Kalyzee. All rights reserved.
//

#include "gstreamer_backend.h"

// Using gstreamer basic tutorial 1 : https://gstreamer.freedesktop.org/documentation/tutorials/basic/hello-world.html

GstElement *pipeline;
GstBus *bus;
GstMessage *msg;
guintptr drawableSurface;
GstVideoOverlay* videoOverlay;

gboolean autoPlay = FALSE;

GstElement *gst_test_get_pipeline() {
    return pipeline;
}

void gst_test_set_auto_play(gboolean _autoPlay) {
    autoPlay = _autoPlay;
}

void gst_test_set_drawable_surface(guintptr _drawableSurface) {
    drawableSurface = _drawableSurface;
    if (videoOverlay)
        gst_video_overlay_prepare_window_handle(videoOverlay);
}

static GstBusSyncReply create_window (GstBus * bus, GstMessage * message, gpointer user_data)
{
    // ignore anything but 'prepare-window-handle' element messages
    if (!gst_is_video_overlay_prepare_window_handle_message (message))
        return GST_BUS_PASS;

    g_print("create_window callback called\n");
    gst_video_overlay_set_window_handle(videoOverlay, drawableSurface);

    gst_message_unref (message);
    return GST_BUS_DROP;
}

void gst_test_prepare_loop(gchar *uri) {
    gchar *launch_command;
    
    if (asprintf(&launch_command,"playbin uri=%s", uri) == -1) {
        g_printerr("Unable to create launch command.");
    }
    
    // launch_command = "videotestsrc ! glimagesink";
    
    pipeline = gst_parse_launch (launch_command, NULL);
    GstElement *video_sink = gst_element_factory_make("glimagesink", "video-sink");
    videoOverlay = GST_VIDEO_OVERLAY(video_sink);
    g_object_set(GST_OBJECT(pipeline), "video-sink", video_sink, NULL);

    bus = gst_element_get_bus (pipeline);
    gst_bus_set_sync_handler(bus, (GstBusSyncHandler)create_window, pipeline, NULL);

    if (autoPlay) {
        gst_element_set_state (pipeline, GST_STATE_PLAYING);
    }
}

void gst_test_run_loop()
{
    /* Wait until error or EOS */
    msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
    gst_object_unref (bus);
    
    /* Free resources */
    if (msg != NULL)
        gst_message_unref (msg);
    
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
}
