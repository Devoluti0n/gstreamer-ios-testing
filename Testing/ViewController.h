//
//  ViewController.h
//  Testing
//
//  Created by Alann on 12/12/2017.
//  Copyright © 2017 Kalyzee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "gstreamer_backend.h"
#import "EaglUiView.h"

@interface ViewController : UIViewController

// Outlets
@property (weak, nonatomic) IBOutlet EaglUiView *drawableView;

// Buttons actions
- (IBAction)nullPressed:(id)sender withEvent:(UIEvent *)event;
- (IBAction)readyPressed:(id)sender withEvent:(UIEvent *)event;
- (IBAction)pausedPressed:(id)sender withEvent:(UIEvent *)event;
- (IBAction)playingPressed:(id)sender withEvent:(UIEvent *)event;

// GST Interactions
- (void)setState:(GstState)state;

@end

